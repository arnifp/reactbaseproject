/**
 * @class
 * @constructor (context)
 * @method create (type, payload)
 */
class ActionCreator {
  constructor(context) {
    this.context = context;
  }

  create(type, payload) {
    return {
      type: `@${this.context}/${type}`,
      payload,
    };
  }
}

export default ActionCreator;
