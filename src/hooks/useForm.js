import { useState } from 'react';

/**
 * Represents a useState for inputs.
 * @constructor
 * @param {any} initialState object, string
 * @returns {Array} [values, handleInputChange, reset]
 */
export const useForm = (initialState = {}) => {
  const [values, setValues] = useState(initialState);

  function reset(newFormState = initialState) {
    setValues(newFormState);
  }

  const handleInputChange = ({ target }) => {
    setValues({
      ...values,
      [target.name]: target.value,
    });
  };

  return [values, handleInputChange, reset];
};
