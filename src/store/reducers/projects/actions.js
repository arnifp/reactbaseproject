import ActionCreator from '@helpers/actionCreator';

const actionCreator = new ActionCreator('Projects');

export default {
  /**
   * @constant
   */
  SET_PROJECT: (payload) => actionCreator.create('SET_PROJECT', payload),
  SELECT_PROJECT: (payload) => actionCreator.create('SELECT_PROJECT', payload),
  UPDATE_PROJECT: (payload) => actionCreator.create('UPDATE_PROJECT', payload),
  DELETE_PROJECT: (payload) => actionCreator.create('DELETE_PROJECT', payload),
};
