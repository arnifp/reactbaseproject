import actions from './actions';

const initialState = {
  projects: [],
  activeProject: false,
};

/**
 * Represents set project.
 * @constructor
 * @param {object} state - state.
 * @param {object} action.payload.
 */
function setProject(state, action) {
  return { ...state, projects: [...state.projects, action.payload] };
}

/**
 * Represents select a project.
 * @constructor
 * @param {object} state - state.
 * @param {object} action.payload - The author of the book.
 */
function selectProject(state, action) {
  return { ...state, activeProject: { ...action.payload } };
}

function deleteProject(state, action) {
  console.log('En el delete', action.payload);
  return {
    ...state,
    activeProject: false,
    projects: state.projects.filter((project) => project.id !== action.payload),
  };
}

function updateProject(state, action) {
  console.log('Desde el update reducer', action.payload.id);
  return {
    ...state,
    projects: state.projects.map((project) =>
      project.id === action.payload.id ? action.payload : project,
    ),
  };
}

export default function projectReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_PROJECT().type:
      return setProject(state, action);

    case actions.SELECT_PROJECT().type:
      return selectProject(state, action);

    case actions.UPDATE_PROJECT().type:
      return updateProject(state, action);

    case actions.DELETE_PROJECT().type:
      return deleteProject(state, action);

    default:
      return state;
  }
}
