import { combineReducers } from 'redux';
import projects from './projects';
import tasks from './tasks';

const rootReducers = combineReducers({ projects, tasks });

export default rootReducers;
