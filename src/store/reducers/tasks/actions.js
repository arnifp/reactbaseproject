import ActionCreator from '@helpers/actionCreator';

const actionCreator = new ActionCreator('Projects');

export default {
  /**
   * @constant
   */
  SET_TASK: (payload) => actionCreator.create('SET_TASK', payload),
  LOAD_TASK: (payload) => actionCreator.create('LOAD_TASK', payload),
  SELECT_TASK: (payload) => actionCreator.create('SELECT_TASK', payload),
  UPDATE_TASK: (payload) => actionCreator.create('UPDATE_TASK', payload),
  COMPLETED_TASK: (payload) => actionCreator.create('COMPLETED_TASK', payload),
  DELETE_TASK: (payload) => actionCreator.create('DELETE_TASK', payload),
};
