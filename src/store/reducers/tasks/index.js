import actions from './actions';

const initialState = {
  tasks: [
    {
      project: 1,
      id: 1,
      description: 'Task uno',
      createdAt: '2020-08-10',
      due: '2020-11-10',
      end: '2020-01-10',
      completed: true,
      tags: [
        {
          id: '12',
          name: 'TagTwo one',
        },
        {
          id: '22',
          name: 'TagTwo two',
        },
      ],
    },
  ],
  activeTask: false,
};

/**
 * Represents a task.
 * @constructor
 * @param {string} state
 * @param {object} action - new Task.
 * @returns {object}
 */
function setTask(state, action) {
  return { ...state, tasks: [...state.tasks, action.payload] };
}

/**
 * Represents a task.
 * @constructor
 * @param {string} state
 * @param {object} action.payload - Task.
 * @returns {object}
 */
function selectTask(state, action) {
  return { ...state, activeTask: { ...action.payload } };
}

/**
 * Represents a task.
 * @constructor
 * @param {string} state
 * @param {object} action.payload - Task.
 * @returns {object}
 */
function deleteTask(state, action) {
  return {
    ...state,
    activeTask: false,
    tasks: state.tasks.filter((task) => task.id !== action.payload),
  };
}

/**
 * Represents a task.
 * @constructor
 * @param {string} state
 * @param {object} action.payload - Task.
 * @returns {object}
 */
function updateTask(state, action) {
  return {
    ...state,
    tasks: state.tasks.map((task) => (task.id === action.payload.id ? action.payload : task)),
  };
}

function completedTask(state, action) {
  return {
    ...state,
    tasks: state.tasks.map((task) => (task.id === action.payload.id ? action.payload : task)),
  };
}

/**
 * Represents a task.
 * @constructor
 * @param {string} state default initialState
 * @param {object} action Task.
 * @returns {object}
 */
export default function taskReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_TASK().type:
      return setTask(state, action);

    case actions.SELECT_TASK().type:
      return selectTask(state, action);

    case actions.UPDATE_TASK().type:
      return updateTask(state, action);

    case actions.COMPLETED_TASK().type:
      return completedTask(state, action);

    case actions.DELETE_TASK().type:
      return deleteTask(state, action);

    default:
      return state;
  }
}
