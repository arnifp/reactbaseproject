import { createStore, compose } from 'redux';
// import { createStore, applyMiddleware, compose } from 'redux';
// import thunk from 'redux-thunk';

import rootReducers from './reducers';

const composeEnhancers =
  (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const store = createStore(rootReducers, composeEnhancers());
// const store = createStore(rootReducers, composeEnhancers(applyMiddleware(thunk)));

export default store;
