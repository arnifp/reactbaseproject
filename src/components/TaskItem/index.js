/* eslint-disable no-unused-vars */
import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

export function TaskItem({ task, onDeleteTask, onUpdateTask }) {
  function handleEditTask() {
    console.log(task.id);
  }

  function handleCompleteTask(e) {
    console.log(e.target.id, e.target.checked);
    const t = {
      ...task,
      completed: e.target.checked,
    };

    console.log(t);

    onUpdateTask(t);
  }

  function handleDeleteTask() {
    onDeleteTask(task.id);
  }

  return (
    <div className='content-card'>
      <div className='content-card-header'>
        <a className='link btn-edit' onClick={handleEditTask}>
          <i className='fa fa-edit'></i>
        </a>
        <span className='switche'>
          <input
            type='checkbox'
            className='input-switche'
            id={task.id}
            onClick={handleCompleteTask}
          />
          <label htmlFor={task.id}>
            <span></span>
          </label>
        </span>
      </div>
      <div className='content-card-body'>
        <div>
          <p>{task.description}</p>
        </div>
        <div className='date-section'>
          <p>
            Created at: <span>{task.createdAt}</span>
          </p>
          <p>
            Due date: <span>{task.due}</span>
          </p>
          <p>
            End Date: <span>{task.end}</span>
          </p>
        </div>
        {/* <div>
          {task.tags.map((tag) => (
            <sl-badge type='primary' pill key={tag.id}>
              {tag.name}
            </sl-badge>
          ))}
        </div> */}
      </div>
      <div className='content-card-footer'>
        <a className='link btn-delete' onClick={handleDeleteTask}>
          <i className='fa fa-trash-alt'></i>
        </a>
      </div>
    </div>
  );
}

TaskItem.propTypes = {
  task: PropTypes.object,
  onDeleteTask: PropTypes.func,
  onUpdateTask: PropTypes.func,
};
