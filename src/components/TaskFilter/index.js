import React from 'react';
import './style.css';
// import { useForm } from 'app/hooks/useForm';

function TaskFilter() {
  function filter(e) {
    console.log(e.target.name, e.target.checked);
  }

  return (
    <div className='content-filter'>
      <span className='filter-title mr-1'>Filter By: </span>
      <div className='input-group'>
        <input type='checkbox' name='due' id='due' onClick={filter} />
        <label htmlFor='due'>Due</label>
      </div>
      <div className='input-group ml-1'>
        <input type='checkbox' name='complete' id='complete' onClick={filter} />
        <label htmlFor='complete'>Complete</label>
      </div>
      <div className='input-group ml-1'>
        <input type='checkbox' name='pending' id='pending' onClick={filter} />
        <label htmlFor='pending'>Pending</label>
      </div>
    </div>
  );
}

export default TaskFilter;
