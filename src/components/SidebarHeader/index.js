import React from 'react';
// import { PropTypes } from 'prop-types';
import { Link } from 'react-router-dom';
import './style.css';

function SidebarHeader() {
  return (
    <div className='sidebar-header'>
      <h4>Arni Flores Pinto</h4>
      <Link to='/login' className='link btn-logout'>
        Loguot
      </Link>
    </div>
  );
}

export default SidebarHeader;

// SidebarHeader.propTypes = {
//   user: PropTypes.func.isRequired,
// };
