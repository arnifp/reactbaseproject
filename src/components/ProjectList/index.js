import React from 'react';
import { PropTypes } from 'prop-types';
import { ProjectItem } from '../ProjectItem';

function ProjectList({ projects, onSelectProject }) {
  return (
    <div className='sidebar-body-list'>
      {projects.map((project) => (
        <ProjectItem
          key={project.id}
          id={project.id}
          name={project.name}
          onSelectProject={onSelectProject}
        />
      ))}
    </div>
  );
}

export default ProjectList;

ProjectList.propTypes = {
  projects: PropTypes.array,
  onSelectProject: PropTypes.func.isRequired,
};
