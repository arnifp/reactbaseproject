import React from 'react';
import { useForm } from '../../hooks/useForm';
import './style.css';

function ProjectForm({ onSetProject }) {
  const [{ project }, handleInputChange, reset] = useForm({ project: '' });

  function isEmpty() {
    return !project.trim() ? true : false;
  }

  const handleAddProject = () => {
    const newProject = {
      id: new Date().getTime(),
      name: project,
    };

    onSetProject(newProject);
    reset();
  };

  return (
    <div className='sidebar-body-input'>
      <input
        type='text'
        className='input input-light mb-1'
        placeholder='New Project'
        autoComplete='off'
        name='project'
        value={project}
        onChange={handleInputChange}
      />
      <button
        type='button'
        className='btn-save-project'
        disabled={isEmpty()}
        onClick={handleAddProject}
      >
        Save project
      </button>
    </div>
  );
}

export default ProjectForm;
