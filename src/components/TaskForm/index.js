import React from 'react';
import PropTypes from 'prop-types';
import { useForm } from '@hooks/useForm';

function TaskForm({ onSetTask }) {
  const [formValues, handleInputChange, reset] = useForm({
    description: '',
    due: '',
    tag: '',
    priority: 'Medium',
  });

  const { description, due, tag, priority } = formValues;

  function isEmpty() {
    return !description.trim() ? true : false;
  }

  function handleAddTask() {
    const newTask = {
      id: new Date().getTime(),
      description,
      createdAt: new Date().toDateString(),
      due,
      tag,
      priority,
    };

    onSetTask(newTask);
    reset();
  }

  return (
    <div className='content-inputs'>
      <input
        type='text'
        className='input input-description mr-2'
        placeholder='Description'
        name='description'
        value={description}
        onChange={handleInputChange}
      />
      <input
        type='date'
        className='input input-date mr-2'
        name='due'
        value={due}
        onChange={handleInputChange}
      />
      {/* <input
        type='text'
        className='input input-tag mr-2'
        placeholder='Tag'
        name='tag'
        value={tag}
        onChange={handleInputChange}
      /> */}

      <div className='radio-select' onChange={handleInputChange} defaultChecked={priority}>
        <input type='radio' id='low' name='priority' value='Low' />
        <label htmlFor='low'>Low</label>
        <input type='radio' id='medium' name='priority' value='Medium' defaultChecked={priority} />
        <label htmlFor='medium'>Medium</label>
        <input type='radio' id='high' name='priority' value='High' />
        <label htmlFor='high'>High</label>
      </div>

      {/* <div className='radio-select-movile'>
        <input type='radio' id='low' name='priority' value='Low'/>
        <label htmlFor='low'>L</label>
        <input type='radio' id='medium' name='priority' value='Medium'/>
        <label htmlFor='medium'>M</label>
        <input type='radio' id='high' name='priority' value='High'/>
        <label htmlFor='high'>H</label>
      </div> */}
      <button className='btn btn-save mb-1' onClick={handleAddTask} disabled={isEmpty()}>
        Save
      </button>
    </div>
  );
}

export default TaskForm;

TaskForm.propTypes = {
  onSetTask: PropTypes.func.isRequired,
};
