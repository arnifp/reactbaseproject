import React, { useEffect, useRef } from 'react';
import { useForm } from '@hooks/useForm';

function ContentHeader({ currentProject, onUpdateProject, onDeleteProject }) {
  const [formValues, handleInputChange, reset] = useForm(currentProject);
  const { name, id } = formValues;
  const activeId = useRef(currentProject.id);

  useEffect(() => {
    if (currentProject.id !== activeId.current) {
      reset(currentProject);
      activeId.current = currentProject.id;
    }
  }, [currentProject, reset]);

  useEffect(() => {
    // console.log(';test', name);
  }, [formValues]);

  function handleEditProject(e) {
    e.preventDefault();
    onUpdateProject(formValues);
    console.log(e);
  }

  function handleDeleteProject() {
    onDeleteProject(id);
  }

  return (
    <div className='content-header'>
      {!currentProject && <i className='fa fa-bars toggle mr-5'></i>}
      {currentProject && (
        <>
          <form onSubmit={handleEditProject}>
            <input
              type='text'
              autoComplete='off'
              className='input input-dark ml-1'
              placeholder='Project name'
              name='name'
              value={name}
              onChange={handleInputChange}
              onBlur={handleEditProject}
            />
          </form>
          <a className='link btn-delete' onClick={handleDeleteProject}>
            <i className='fa fa-trash-alt'></i>
            Delete projects
          </a>
        </>
      )}
    </div>
  );
}

export default ContentHeader;
