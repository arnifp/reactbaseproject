import React from 'react';
import PropTypes from 'prop-types';
import { TaskItem } from '../TaskItem';

// const tasks = [
//   {
//     id: '1',
//     description: 'Task uno',
//     createdAt: '2020-08-10',
//     due: '2020-11-10',
//     end: '2020-01-10',
//     completed: true,
//     tags: [
//       {
//         id: '12',
//         name: 'TagTwo one',
//       },
//       {
//         id: '22',
//         name: 'TagTwo two',
//       },
//     ],
//   },
//   {
//     id: '2',
//     description: 'Task Dos',
//     createdAt: '2020-07-10',
//     due: '2020-05-10',
//     end: '2020-09-10',
//     completed: false,
//     tags: [
//       {
//         id: '1',
//         name: 'TagOne one',
//       },
//       {
//         id: '2',
//         name: 'TagOne two',
//       },
//       {
//         id: '3',
//         name: 'TagOne three',
//       },
//     ],
//   },
// ];

function TaskList({ tasks, onDeleteTask, onUpdateTask }) {
  return (
    <div className='content-body'>
      {tasks.map((task) => (
        <TaskItem
          key={task.id}
          task={task}
          onDeleteTask={onDeleteTask}
          onUpdateTask={onUpdateTask}
        />
      ))}
    </div>
  );
}

export default TaskList;

TaskList.propTypes = {
  tasks: PropTypes.array,
  onDeleteTask: PropTypes.func,
  onUpdateTask: PropTypes.func,
};
