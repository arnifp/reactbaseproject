import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

export function ProjectItem({ id, name, onSelectProject }) {
  function handleSelectProject() {
    onSelectProject({ id, name });
  }

  return (
    <div className='card pointer' onClick={handleSelectProject}>
      <p className='project-title'>{name}</p>
      <div className='project-description'>{id}</div>
    </div>
  );
}

ProjectItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  onSelectProject: PropTypes.func.isRequired,
};
