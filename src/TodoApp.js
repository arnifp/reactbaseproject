import React from 'react';
import { Provider } from 'react-redux';

import store from './store';
import { AppRouter } from './routers';
import './TodoApp.css';

function TodoApp() {
  return (
    <Provider store={store}>
      <AppRouter />
    </Provider>
  );
}

export default TodoApp;
