import React from 'react';
import { Link } from 'react-router-dom';
import './style.css';

export function Login() {
  return (
    <div className='ui-login'>
      <h1>Login</h1>

      <form action='' className='form-login'>
        <div className='group-unput-login'>
          <label>Email</label>
          <input type='text' />
        </div>
        <div className='group-unput-login'>
          <label>Password</label>
          <input type='text' />
        </div>
        <Link className='btn pointer btn-login' to='/todo'>
          Start ToDo List
        </Link>
      </form>
    </div>
  );
}
