import React from 'react';
import './style.css';

function NothingSelected() {
  function handleShowMenu() {
    const drawer = document.querySelector('.drawer-placement-left');
    const openButton = document.querySelector('.toggle');
    // const openButton = drawer.nextElementSibling;
    // const closeButton = drawer.querySelector('sl-button[type="primary"]');
    openButton.addEventListener('click', () => drawer.show());
    // closeButton.addEventListener('click', () => drawer.hide());
  }
  return (
    <>
      <sl-drawer label='Projects' placement='left' class='drawer-placement-left'>
        {/* <div class='sidebar-header'>
            <h4>Arni Flores Pinto</h4>
            <a href='#' class='link'>Loguot</a>
          </div>
          <div class='sidebar-body'>
            <div class='sidebar-body-input'>
              <input type='text' class='input input-light mb-1' placeholder='New Project'>
              <button disabled>Save project</button>
            </div>
            <div class='sidebar-body-list'>
              <div class='card'>
                <p>Title</p>
                <div>
                6
              </div>
            </div>
            <div class='card'></div>
          </div>
        </div> */}
      </sl-drawer>
      <div className='show-menu'>
        <i className='fa fa-bars toggle mr-5' onClick={handleShowMenu}></i>
      </div>
      <div className='nothing'>
        <h1>NothingSelected</h1>
        <span className='App-logo'></span>
      </div>
    </>
  );
}

export default NothingSelected;
