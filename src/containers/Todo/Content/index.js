import React from 'react';
import { connect } from 'react-redux';

import projectActions from '@reducers/projects/actions';
import taskActions from '@reducers/tasks/actions';
import ContentHeader from '@components/ContentHeader';
import TaskForm from '@components/TaskForm';
import TaskFilter from '@components/TaskFilter';
import TaskList from '@components/TaskList';
import NothingSelected from '../NothingSelected/index';
import './style.css';

function Content({
  activeProject,
  updateProject,
  deleteProject,
  setTask,
  tasks,
  deleteTask,
  updateTask,
}) {
  return (
    <section className='content'>
      {activeProject ? (
        <>
          <ContentHeader
            currentProject={activeProject}
            onUpdateProject={updateProject}
            onDeleteProject={deleteProject}
          />
          <TaskForm onSetTask={setTask} />
          <TaskFilter />
          <TaskList tasks={tasks} onDeleteTask={deleteTask} onUpdateTask={updateTask} />
        </>
      ) : (
        <NothingSelected />
      )}
    </section>
  );
}

/**
 * Get state (projects, active)
 * @param {Object} state
 */
function mapStateToProps(state) {
  const { activeProject } = state.projects;
  const { tasks } = state.tasks;
  return { activeProject, tasks };
}

/**
 * Set project ({name, id})
 * @param {Function} dispatch
 */
function mapDispatchToProps(dispatch) {
  function deleteProject(project) {
    dispatch(projectActions.DELETE_PROJECT(project));
  }

  function updateProject(project) {
    dispatch(projectActions.UPDATE_PROJECT(project));
  }

  function setTask(task) {
    dispatch(taskActions.SET_TASK(task));
  }

  function deleteTask(task) {
    dispatch(taskActions.DELETE_TASK(task));
  }

  function updateTask(task) {
    dispatch(taskActions.UPDATE_TASK(task));
  }

  return { updateProject, deleteProject, setTask, deleteTask, updateTask };
}

export default connect(mapStateToProps, mapDispatchToProps)(Content);
