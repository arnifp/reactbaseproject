import React from 'react';
import { connect } from 'react-redux';

import SidebarHeader from '@components/SidebarHeader';
import ProjectForm from '@components/ProjectForm';
import ProjectList from '@components/ProjectList';
import projectActions from '@reducers/projects/actions';
import './style.css';

// eslint-disable-next-line no-unused-vars
function Sidebar({ projects, active, setProject, selectProject }) {
  function handleSetProject(project) {
    setProject(project);
  }

  return (
    <aside className='sidebar'>
      <SidebarHeader />
      <div className='sidebar-body'>
        <ProjectForm onSetProject={handleSetProject} />
        <ProjectList projects={projects} onSelectProject={selectProject} />
      </div>
    </aside>
  );
}

/**
 * Get state (projects, active)
 * @param {Object} state
 */
function mapStateToProps(state) {
  const { projects, active } = state.projects;
  return { projects, active };
}

/**
 * Set project ({name, id})
 * @param {Function} dispatch
 */
// eslint-disable-next-line no-unused-vars
function mapDispatchToProps(dispatch) {
  function setProject(project) {
    dispatch(projectActions.SET_PROJECT(project));
  }

  function selectProject(project) {
    dispatch(projectActions.SELECT_PROJECT(project));
  }

  return { setProject, selectProject };
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
