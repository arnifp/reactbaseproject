import React from 'react';

import './style.css';
import Sidebar from './Sidebar';
import Content from './Content';

export default function Todo() {
  return (
    <div className='main'>
      <Sidebar />
      <Content />
    </div>
  );
}
