import { Login } from '@containers/Login';
import Todo from '@containers/Todo';
import React from 'react';
import { BrowserRouter as Router, Switch, Redirect, Route } from 'react-router-dom';

export function AppRouter() {
  return (
    <Router>
      <Switch>
        <Route path='/login' component={Login} />
        <Route path='/todo' component={Todo} />
        <Redirect from='/' to='/todo' />
      </Switch>
    </Router>
  );
}
