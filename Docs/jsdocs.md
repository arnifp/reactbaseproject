# jsdocs

## Example

```js
import React from 'react';
/**
 * Test Component.
 * @constructor
 * @param {string} title - The title of the Test component.
 * @param {string} author - The author of the Test component.
 */

export function Test({title, author}) {
  return (
    <div>
      <p>
        {title}, {author}
      </p>
    </div>
  );
}
```

## Generate docs

run:
`npm run docs`

### References

[Config in your project](https://www.inkoop.io/blog/a-guide-to-js-docs-for-react-js/)

[Use](https://jsdoc.app/about-getting-started.html#adding-documentation-comments-to-your-code)
