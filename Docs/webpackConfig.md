# WebPack Config

Start alias configuration
`$ npm run eject`

## Implement Alias

[Config File](../config/webpack.config.js)

Root project
`./config/webpack.config.js`

```js
alias: {
  // You can add more settings here
  'app': path.resolve(__dirname, '../src'),
  'images': path.resolve(__dirname, '../img'),
  'react-native': 'react-native-web',
  ...(isEnvProductionProfile && {
    'react-dom$': 'react-dom/profiling',
    'scheduler/tracing': 'scheduler/tracing-profiling',
  }),
  ...(modules.webpackAliases || {}),
},
```

## VSCODE Config

[Config File](../jsconfig.json)

Root
`./jsconfig.json`

> This setting is only for vscode

```json
{
  "compilerOptions": {
    "target": "es2017",
    "allowSyntheticDefaultImports": false,
    "baseUrl": "./",
    "paths": {
      "app/*": ["src/*"],
      "images/*": ["img/*"]
    }
  },
  "exclude": ["node_modules", "dist"]
}
```
