# Config

npm i -D eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react

npm i -D eslint prettier babel-eslint eslint-config-prettier eslint-plugin-prettier

`https://thomlom.dev/setup-eslint-prettier-react/`

```json
{
  "printWidth": 100,
  "trailingComma": "all",
  "tabWidth": 2,
  "semi": true,
  "singleQuote": true,
  "jsxSingleQuote": true,
  "bracketSpacing": false
}


{
  "parser": "babel-eslint",
  "env": {
    "browser": true,
    "jest": true,
    "es6": true
  },
  "plugins": ["react", "import"],
  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module",
    "ecmaFeatures": {
        "jsx": true
    }
  },
  "settings": {
    "react": {
        "version": "detect"
    }
  },
  "extends": [
    "airbnb",
    "prettier",
    "prettier/react",
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  "rules": {
    "import/first": "error",
    "no-console": "warn"
  }
}
```
